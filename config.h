/* See LICENSE file for copyright and license details. */

/* appearance */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating = 0;  /* 1 means swallow floating windows by default */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static int linepx             = 2;        /* 0 means no underline */
static const char *fonts[]    = { "Ttyp0:size=9" };
static char normbgcolor[]     = "#222222";
static char normbordercolor[] = "#444444";
static char normfgcolor[]     = "#bbbbbb";
static char ltsbgcolor[]      = "#bbbbbb";
static char selfgcolor[]      = "#eeeeee";
static char selbordercolor[]  = "#005577";
static char selbgcolor[]      = "#005577";
static char *colors[][4] = {
       /*               fg           bg           border   */
       [SchemeNorm]  = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]   = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeTitle] = { normfgcolor, normbgcolor, normbordercolor  },
       [SchemeLts] =   { normfgcolor, ltsbgcolor,  normbordercolor  },
};

/* tagging */
static const char *tags[] = { "Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ", "Ⅵ", "Ⅶ" };
/* static const char *tags[] = { "а", "б", "в", "г", "д", "е", "ё" }; */

typedef struct {
	const char *name;
	const void *cmd;
} Sp;

const char *spumpv[] = {"umpv", NULL };
const char *spmusic[] = {"st", "-c", "music", "-e", "music", NULL };
const char *spterm[] = {"st", "-c", "scratch", NULL };

static Sp scratchpads[] = {
	/* name          cmd  */
	{"spmusic", spmusic},
	{"spterm",  spterm},
	{"spumpv",  spumpv},
};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class | instance | title | tags mask | isfloating | isterminal | noswallow |
	 * monitor | scratchkey */
	{ "Gimp",     NULL,   NULL,           0, 1, 0,  0, -1, },
	{ NULL,       "st",   NULL,           0, 0, 1, -1, -1, },
	{ NULL,       NULL,   "Event Tester", 0, 1, 0,  1, -1, },
	{ NULL,       NULL,   "Media viewer", 0, 1, 0,  0, -1, },
	{ NULL,       NULL,   "floating",     0, 1, 1,  0, -1, },
	{ "music",    NULL,   NULL,    SPTAG(0), 1, 1,  0, -1, },
	{ "scratch",  NULL,   NULL,    SPTAG(1), 1, 1,  0, -1, },
	{ NULL,      "umpv",  NULL,    SPTAG(2), 1, 1,  0, -1, },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "",      tile },    /* first entry is default */
	{ "",      bstack },
	{ "[M]",    monocle },
	{ "",      NULL },    /* no layout function means floating behavior */
	{ NULL,     NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask,              KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

#define TERMINAL "st"
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
#define TSHCMD(cmd) SHCMD(TERMINAL " " cmd)

#define STATUSBAR "sblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "ltsbgcolor",         STRING,  &ltsbgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		{ "borderpx",          	INTEGER, &borderpx },
		{ "snap",          		INTEGER, &snap },
		{ "showbar",          	INTEGER, &showbar },
		{ "topbar",          	INTEGER, &topbar },
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	 	FLOAT,   &mfact },
};

#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key        function        argument */
	
	/* spawning programs */
	{ MODKEY,           XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,           XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,           XK_Down,   spawn,          SHCMD("light -U 1") },
	{ MODKEY,           XK_Left,   spawn,          SHCMD("cmus-remote -r") },
	{ MODKEY,           XK_Right,  spawn,          SHCMD("cmus-remote -n") },
	{ MODKEY,           XK_Up,     spawn,          SHCMD("light -A 1") },
	{ MODKEY,           XK_e,      spawn,          TSHCMD("lfp") },
	{ MODKEY|ShiftMask, XK_m,      spawn,          TSHCMD("neomutt") },
	{ MODKEY,           XK_s,      spawn,          SHCMD("$BROWSER > /dev/null") },
	{ MODKEY|ShiftMask, XK_s,      spawn,          SHCMD("tabbed -c vimb -p misc -e") },
	{ MODKEY|ShiftMask, XK_t,      spawn,          SHCMD("telegram-desktop") },
	{ MODKEY,           XK_x,      spawn,          SHCMD("slock") },
	{ MODKEY,           XK_y,      spawn,          SHCMD("dmenu_search") },
	/* scratchpads */
	{ MODKEY,           XK_m,      togglescratch,  {.ui = 0 } }, /* cmus     */
	{ MODKEY,           XK_o,      togglescratch,  {.ui = 1 } }, /* terminal */
	{ MODKEY,           XK_u,      togglescratch,  {.ui = 2 } }, /* umpv     */
	/* screen shots and recording */
	{ MODKEY,           XK_Print,  spawn,          SHCMD("capture shot full") },
	{ MODKEY|ShiftMask, XK_Print,  spawn,          SHCMD("capture shot sel tmp") },
	{ MODKEY,           XK_r,      spawn,          SHCMD("capture vid full") },
	{ MODKEY|ShiftMask, XK_r,      spawn,          SHCMD("capture vid sel tmp") },
	/* window manipulation */
	{ MODKEY,           XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,           XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask, XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask, XK_k,      movestack,      {.i = -1 } },
	{ MODKEY,           XK_comma,  incnmaster,     {.i = +1 } },
	{ MODKEY,           XK_period, incnmaster,     {.i = -1 } },
	{ MODKEY,           XK_h,      setmfact,       {.f = -0.005} },
	{ MODKEY,           XK_l,      setmfact,       {.f = +0.005} },
	{ MODKEY|ShiftMask, XK_Return, zoom,           {0} },
	{ MODKEY,           XK_Tab,    view,           {0} },
	{ MODKEY,           XK_q,      killclient,     {0} },
	/* layouts */
	{ MODKEY,           XK_space,  cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask, XK_space,  cyclelayout,    {.i = -1 } },
	{ MODKEY,           XK_t,      togglefloating, {0} },
	{ MODKEY,           XK_b,      togglebar,      {0} },
	/* multimedia keys */
	{ 0, XF86XK_AudioMute,         spawn, SHCMD("vol mute") },
	{ 0, XF86XK_AudioMicMute,      spawn, SHCMD("amixer -q sset Capture toggle") },
	{ 0, XF86XK_AudioRaiseVolume,  spawn, SHCMD("vol up 5") },
	{ 0, XF86XK_AudioLowerVolume,  spawn, SHCMD("vol down 5") },
	{ 0, XF86XK_AudioPrev,         spawn, SHCMD("cmus-remote -r") },
	{ 0, XF86XK_AudioNext,         spawn, SHCMD("cmus-remote -n") },
	{ 0, XF86XK_AudioPlay,         spawn, SHCMD("cmus-remote -u") },
	{ 0, XF86XK_Launch1,           spawn, TSHCMD("-t floating -e alsamixer") },
	{ 0, XF86XK_MonBrightnessUp,   spawn, SHCMD("light -A 5") },
	{ 0, XF86XK_MonBrightnessDown, spawn, SHCMD("light -U 5") },
	{ 0, XF86XK_ScreenSaver,       spawn, SHCMD("slock") },
	/* tags */
	{ MODKEY,           XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask, XK_0,      tag,            {.ui = ~0 } },
	TAGKEYS(            XK_1,                      0)
	TAGKEYS(            XK_2,                      1)
	TAGKEYS(            XK_3,                      2)
	TAGKEYS(            XK_4,                      3)
	TAGKEYS(            XK_5,                      4)
	TAGKEYS(            XK_6,                      5)
	TAGKEYS(            XK_7,                      6)
	{ MODKEY|ShiftMask, XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        cyclelayout,    {.i = +1 } },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[0]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigstatusbar,   {.i = 6} },
	{ ClkStatusText,        ShiftMask,      Button3,        sigstatusbar,   {.i = 7} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

